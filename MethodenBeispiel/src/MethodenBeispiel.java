public class MethodenBeispiel {
    public static void main(String[] args) {

        int zahl1 = 5;
        int zahl2 = 7;

        int erg = min(zahl1,zahl2);

        int erg2 = max(3,6,2);

        System.out.println(Aufgaben.BMIClassification(67,182,"m"));

    }

    static Integer min(int zahl1, int zahl2){
        if(zahl1<zahl2) return zahl1;
        else return zahl2;
    }

    static Integer max(int zahl1, int zahl2, int zahl3){
        if(zahl1<zahl2) {
            if (zahl2 < zahl3) return zahl3;
            else return zahl2;
        }
        else{
            if(zahl1>zahl3) return zahl1;
            else return zahl3;
        }
    }


}
