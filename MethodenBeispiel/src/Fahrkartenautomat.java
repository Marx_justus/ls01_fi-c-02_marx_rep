//Justus Marx
//Bitbucket Repository
import java.text.DecimalFormat;
import java.util.Scanner;
import java.lang.Math;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
        rueckgeldAusgeben();
    }
    static double fahrkartenbestellungErfassen(){
        int anzahl;
        double zuZahlenderBetrag;
        Scanner tastatur = new Scanner(System.in);
        System.out.print("Zu zahlender Betrag (EURO): ");
        zuZahlenderBetrag = tastatur.nextDouble();
        //Abrage der Anzahl der Tickets Aufgabe 03
        System.out.println("Anzahl der Tickets:");
        anzahl = tastatur.nextInt();
        return zuZahlenderBetrag*anzahl;
    }
    static double fahrkartenBezahlen(double zuZahlen){
        // Geldeinwurf
        // -----------
        Scanner tastatur = new Scanner(System.in);
        double eingeworfeneMünze;
        double eingezahlterGesamtbetrag = 0.0;
        DecimalFormat df = new DecimalFormat("#0.00");
        while(eingezahlterGesamtbetrag < zuZahlen)
        {
            //Aufgabe 02:
            System.out.println("Noch zu zahlen: " + df.format((zuZahlen - eingezahlterGesamtbetrag)) + "Euro");
            System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
            eingeworfeneMünze = tastatur.nextDouble();
            eingezahlterGesamtbetrag += eingeworfeneMünze;
        }
        return eingezahlterGesamtbetrag - zuZahlen;
    }
    static void fahrkartenAusgeben(){
        // Fahrscheinausgabe
        // -----------------
        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
            System.out.print("=");
            try {
                Thread.sleep(250);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        System.out.println("\n\n");
    }
    static void rueckgeldAusgeben(){
        double rückgabebetrag;
        DecimalFormat df = new DecimalFormat("#0.00");

        // Rückgeldberechnung und -Ausgabe
        // -------------------------------
        rückgabebetrag = fahrkartenBezahlen(fahrkartenbestellungErfassen());
        if(rückgabebetrag > 0.0)
        {
            System.out.printf("Der Rückgabebetrag in Höhe von " + df.format(rückgabebetrag) + " EURO");
            System.out.println(" wird in folgenden Münzen ausgezahlt:");

            while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
            {
                System.out.println("2 EURO");
                rückgabebetrag -= 2.0;
            }
            while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
            {
                System.out.println("1 EURO");
                rückgabebetrag -= 1.0;
            }
            while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
            {
                System.out.println("50 CENT");
                rückgabebetrag -= 0.5;
            }
            while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
            {
                System.out.println("20 CENT");
                rückgabebetrag -= 0.2;
            }
            while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
            {
                System.out.println("10 CENT");
                rückgabebetrag -= 0.1;
            }
            while(rückgabebetrag >= 0.049)// 5 CENT-Münzen
            {
                System.out.println("5 CENT");
                rückgabebetrag -= 0.049;
            }

        }

        System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                "vor Fahrtantritt entwerten zu lassen!\n"+
                "Wir wünschen Ihnen eine gute Fahrt.");
    }
}