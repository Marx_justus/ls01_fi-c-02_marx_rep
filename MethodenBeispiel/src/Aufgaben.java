public class Aufgaben {

    static String intToMonth1(int month){
        if(month==1) return "Januar";
        if(month==2) return "Februar";
        if(month==3) return "März";
        if(month==4) return "April";
        if(month==5) return "Mai";
        if(month==6) return "Juni";
        if(month==7) return "Juli";
        if(month==8) return "August";
        if(month==9) return "September";
        if(month==10) return "Oktober";
        if(month==11) return "November";
        if(month==12) return "Dezember";
        else return "Ungültige Eingabe!";
    }
    static String intToMonth2(int month){
        switch (month){
            case 1: return "Januar";
            case 2: return "Februar";
            case 3: return "März";
            case 4: return "April";
            case 5: return "Mai";
            case 6: return "Juni";
            case 7: return "Juli";
            case 8: return "August";
            case 9: return "September";
            case 10: return "Oktober";
            case 11: return "November";
            case 12: return "Dezember";
            default: return "Falsche Eingabe!";
        }
    }
    static String BMIClassification(double weigth, double heigth, String gender){
        double bmi = (weigth)/((heigth/100)*(heigth/100));
        switch (gender){
            case "m":
                if(bmi<=25 && bmi>=20) return "Bmi: "+bmi+"\n Normalgewicht";
                else if(bmi<20) return "Bmi: "+bmi+"\n Untergewicht";
                else return "Bmi: "+bmi+"\n Übergewicht";
            case "w":
                if(bmi<=24 && bmi>=19) return "Bmi: "+bmi+"\n Normalgewicht";
                else if(bmi<19) return "Bmi: "+bmi+"\n Untergewicht";
                else return "Bmi: "+bmi+"\n Übergewicht";
            default:return "Fehler";
        }
    }
}
