public class ArrayHelper {
    public static void main(String[] args) {
        int[] zahlen = {2,3,4,5,67,8};
        System.out.println(convertArrayToString(zahlen));
    }

    private static String convertArrayToString(int[] zahlen) {
        String out = "";
        for (int i : zahlen)
            out+=i+",";
        return out.substring(0,out.length()-1);
    }
}
