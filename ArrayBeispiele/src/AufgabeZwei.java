public class AufgabeZwei {
    public static void main(String[] args) {
        int[] zahlen = {1,2,3,4,5};
        revert(zahlen);
    }
    public static void revert(int[] liste){
        for(int i=0; i<liste.length/2; i++){
            int j= liste[i];
            liste[i] = liste[liste.length -i -1];
            liste[liste.length -i -1] = j;
        }
        for (int i : liste)
            System.out.print(i+",");
    }
}
