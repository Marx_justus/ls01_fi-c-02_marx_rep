public class Lotto {
    public static void main(String[] args) {
        int[] gewinn = {3,7,12,18,37,42};
        runTest(12,gewinn);
        runTest(13,gewinn);
    }
    static void runTest(int x,int[] gewinn){
        if(test(x,gewinn))
            System.out.println("Die Zahl "+ x + " ist in der ziehung enthalten.");
        else
            System.out.println("Die Zahl "+ x + " ist nicht in der ziehung enthalten.");
    }
    static boolean test(int x,int[] gewinn){
        for (int i : gewinn){
            if (i==x)
                return true;
        }
        return false;
    }
}
