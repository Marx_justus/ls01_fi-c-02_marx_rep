import java.util.Scanner;

public class Temparaturumrechnung {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.println("Start in Clesius: ");
        double start = s.nextDouble();
        System.out.println("Ende in Clesius: ");
        double ende = s.nextDouble();
        System.out.println("Schrittweite: ");
        double schrittweite = s.nextDouble();
        calculate(start,ende,schrittweite);
    }

    static void calculate(double start, double ende,double schrittweite){
        for(double x = start;x<ende; x+=schrittweite){
            System.out.printf("%-8.2f%s%8.2f%s\n",x, "°C", (x*9/5+32),"°F");
        }
    }
}
