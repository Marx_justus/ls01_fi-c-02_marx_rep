import java.util.Scanner;

public class SumA {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int n = s.nextInt();
        int count = 1;
        int erg = 0;
        while (count<=n){
            erg+=count;
            count++;
        }
        System.out.println(erg);
    }
}
