import java.util.Scanner;

public class SumD {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int n = s.nextInt();
        int count = 1;
        int erg = 0;
        while (count<=(2*n+1)-2*n){
            if(count%2==0) erg-=count;
            else erg+=count;
            count += 1;
        }
        System.out.println(erg);
    }
}
