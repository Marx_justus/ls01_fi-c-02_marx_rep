import java.util.Scanner;

public class SumC {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int n = s.nextInt();
        int count = 1;
        int erg = 0;
        while (count<=(2*n)+1){
            erg+=count;
            count += 2;
        }
        System.out.println(erg);
    }
}
