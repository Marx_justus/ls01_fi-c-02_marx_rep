import java.util.Scanner;

public class QuotSub {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.println("Divident: ");
        int divident = s.nextInt();
        System.out.println("Divisor: ");
        int divisor = s.nextInt();
        int quotient = 0;
        while (divident>=divisor){
            quotient++;
            divident-=divisor;
        }
        System.out.println("Quotient: "+quotient+" Rest: "+divident);

    }
}
