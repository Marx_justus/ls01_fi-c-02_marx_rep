import java.util.Scanner;

public class SumB {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int n = s.nextInt();
        int count = 2;
        int erg = 0;
        while (count<=2*n){
            erg+=count;
            count += 2;
        }
        System.out.println(erg);
    }
}
