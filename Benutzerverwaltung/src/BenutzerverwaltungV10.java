import java.io.*;
import java.util.Scanner;

class BenutzerverwaltungV10{

    private static BenutzerListe benutzerListe;
    public static void start() throws Exception {
        benutzerListe = new BenutzerListe();

        benutzerListe.insert(new Benutzer("Paula", Crypto.encrypt("paula".toCharArray())));
        benutzerListe.insert(new Benutzer("Adam37", Crypto.encrypt("adam37".toCharArray())));
        benutzerListe.insert(new Benutzer("Darko", Crypto.encrypt("darko".toCharArray())));
        benutzerListe.insert(new Benutzer("Admin", new char[]{36, 61, 72, 72, 75, 13, 14, 15}));

        ObjectOutputStream benutzerdatei1 = new ObjectOutputStream(new FileOutputStream("Benutzer.bin"));
        benutzerdatei1.writeObject(benutzerListe);
        benutzerdatei1.close();

        ObjectInputStream benutzerdatei = new ObjectInputStream(new FileInputStream("Benutzer.bin"));
        BenutzerListe benutzerListe = (BenutzerListe)benutzerdatei.readObject();
        benutzerdatei.close();
        System.out.println("-test->\n" + benutzerListe.select() + "<-test-\n");

        System.out.println("Anmelden: 1 \nRegistrieren: 2");
        Scanner s = new Scanner(System.in);
        switch (s.nextInt()){
            case 1:
                Boolean tmp = true;
                do {
                    System.out.println("Benutzername: ");
                    String usn = s.next();
                    System.out.println("Passwort: ");
                    String pwd = s.next();
                    if (authenticate(usn,Crypto.encrypt(pwd.toCharArray()))) {
                        //Ich kann keine Anmeldedaten(Status, letzte Anmeldung finden) insofern ist die Aufgabe missverständlich
                        System.out.println("Erfolgreiche Anmeldung!");
                        tmp = false;
                    }else
                        System.out.println("Falscher Name oder falsches Passwort");
                }while (tmp);
                break;
            case 2:
                Boolean temp = true;
                do {
                    System.out.println("Benutzername: ");
                    String usn = s.next();
                    System.out.println("Passwort: ");
                    String pwd = s.next();
                    System.out.println("Passwort wiederholen: ");
                    String pwd2 = s.next();
                    if (benutzerListe.select(usn).equals("")&&pwd.equals(pwd2)){
                        benutzerListe.insert(new Benutzer(usn,Crypto.encrypt(pwd.toCharArray())));
                        System.out.println("Erfolgreiche Registrierung!");
                        temp = false;
                    }else
                        System.out.println("Nutzername existiert bereits!");
                }while (temp);
                break;
            default: System.err.println("Falsche Eingabe");
        }
        System.out.println("-test->\n" + benutzerListe.select() + "<-test-\n");
        benutzerdatei1 = new ObjectOutputStream(new FileOutputStream("Benutzer.bin"));
        benutzerdatei1.writeObject(benutzerListe);
        benutzerdatei1.close();
    }

    public static boolean authenticate(String name, char[] cryptoPw) {
        Benutzer b = benutzerListe.getBenutzer(name);
        if(b != null) {
            if(b.hasPasswort(cryptoPw)){
                return true;
            }
        }
        return false;
    }
}


