import java.util.Scanner;

public class Mittelwert {

   public static void main(String[] args) {

      // (E) "Eingabe"
      // Werte f�r x und y festlegen:
      // ===========================
      double zahl = 0;
      double count = 0;
      double m;
      char antwort = 'j';

      Scanner scan = new Scanner(System.in);
      while (antwort=='j') {
         System.out.print("Bitte geben Sie eine Zahl ein: ");
         zahl += scan.nextDouble();
         System.out.println("Wollen sie weiter machen?(j/n)");
         antwort = scan.next().charAt(0);
         count++;
      }

      
      // (V) Verarbeitung
      // Mittelwert von x und y berechnen: 
      // ================================
      m = zahl / count;
      
      // (A) Ausgabe
      // Ergebnis auf der Konsole ausgeben:
      // =================================
      System.out.printf("Der Mittelwert ist %.3f\n", m);
   }
}
