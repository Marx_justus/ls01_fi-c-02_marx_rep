class Crypto {
    private static int cryptoKey = 65500;

    public static char[] encrypt(char[] s) {
        char[] encrypted = new char[s.length];
        for (int i = 0; i < s.length; i++) {
            encrypted[i] = (char) ((s[i] + cryptoKey) % 128);
        }
        return encrypted;
    }
}
