import java.io.Serializable;
import java.util.Arrays;

class Benutzer implements Serializable {
    private String name;
    private char[] passwort;  // Verschlüsselt!

    private Benutzer next;

    public Benutzer(String name, char[] pw) {
        this.name = name;
        this.passwort = pw;

        this.next = null;
    }

    public boolean hasName(String name) {
        return name.equals(this.name);
    }

    public boolean hasPasswort(char[] cryptoPw) {
        return Arrays.equals(this.passwort, cryptoPw);
    }

    public String toString() {
        String s = "";
        s += name + " ";
        s += passwort;
        return s;
    }

    public Benutzer getNext() {
        return next;
    }

    public void setNext(Benutzer b) {
        next = b;
    }
}
