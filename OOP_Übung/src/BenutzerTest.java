public class BenutzerTest {
    public static void main(String[] args) {
        Benutzer leon = new Benutzer("Leon", "qwe", "leon@gmail.com");
        Benutzer werni = new Benutzer("Wener", "123456", "wernia@gmx.com");
        System.out.println("Aktueller Name: " + leon.getName());
        System.out.println("Aktuelles Passwort: " +leon.getPassword());
        System.out.println("Aktuelle Email: " +leon.getEmail());
        System.out.println("Name zu Leonidas verändert");
        leon.setName("Leonidas");
        System.out.println("Ausgabe des neuen Namen: " + leon.getName());
        System.out.println("Passwort zu 123 verändert");
        leon.setPassword("123");
        System.out.println("Ausgabe des neuen Passworts: " + leon.getPassword());
        System.out.println(leon.login("123","leon@gmail.com"));
        System.out.println(leon.getLastLogin());
        leon.logout();
        System.out.println(werni.getEmail());
    }
}
