import java.time.LocalDate;

public class Benutzer {

    private String name;
    private String password;
    private String email;
    private String adresse;
    private int berechtigungsstatus;
    private boolean anmeldestatus;
    private String lastLogin;

    public Benutzer(String name, String password, String email) {
        this.name = name;
        this.password = password;
        this.email = email;
        this.anmeldestatus = false;
    }

    public boolean login(String password, String email){
        if (this.password.equals(password)&&this.email.equals(email)){
            anmeldestatus = true;
            lastLogin = LocalDate.now().toString();
        }
        return anmeldestatus;
    }

    public void logout(){
        anmeldestatus=false;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public int getBerechtigungsstatus() {
        return berechtigungsstatus;
    }

    public void setBerechtigungsstatus(int berechtigungsstatus) {
        this.berechtigungsstatus = berechtigungsstatus;
    }

    public boolean getAnmeldestatus() {
        return anmeldestatus;
    }

    public String getLastLogin() {
        return lastLogin;
    }
}
