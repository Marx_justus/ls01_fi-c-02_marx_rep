//Justus Marx
//Bitbucket Repository
import java.text.DecimalFormat;
import java.util.Scanner;
import java.lang.Math;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
        rueckgeldAusgeben();
    }
    static double fahrkartenbestellungErfassen(){
        int anzahl;
        DecimalFormat df = new DecimalFormat("#0.00");
        Scanner tastatur = new Scanner(System.in);
        String[] fahrkarten_bez = new String[]{"Einzelfahrschein Berlin AB ","Einzelfahrschein Berlin BC ","Einzelfahrschein Berlin ABC ","Kurzstrecke",
        "Tageskarte Berlin AB","Tageskarte Berlin BC","Tageskarte Berlin ABC","Kleingruppen-Tageskarte Berlin AB","Kleingruppen-Tageskarte Berlin BC",
        "Kleingruppen-Tageskarte Berlin ABC"};
        double[] fahrkarten_preis = new double[]{2.90,3.30,3.60,1.90,8.60,9.00,9.60,23.50,24.30,24.90};
        System.out.print("Wählen Sie ihre Wunschfahrkarte für Berlin AB aus:\n");
        for (int i = 0; i < fahrkarten_bez.length; i++) {
            System.out.println(fahrkarten_bez[i]+" ["+df.format(fahrkarten_preis[i])+"€] ("+(i+1)+")");
        }
        System.out.println("Ihre Wahl: ");
        double zuZahlenderBetrag = 0;
        int choice = tastatur.nextInt();
        while (choice < 1 || choice > fahrkarten_bez.length){
            System.out.print("falsche Eingabe\n"+
                    "Ihre Wahl: ");
            choice = tastatur.nextInt();
        }
        zuZahlenderBetrag = fahrkarten_preis[choice-1];
        //Abrage der Anzahl der Tickets Aufgabe 03
        System.out.println("Anzahl der Tickets:");
        anzahl = tastatur.nextInt();
        return zuZahlenderBetrag*anzahl;
    }
    static double fahrkartenBezahlen(double zuZahlen){
        // Geldeinwurf
        // -----------
        Scanner tastatur = new Scanner(System.in);
        double eingeworfeneMünze;
        double eingezahlterGesamtbetrag = 0.0;
        DecimalFormat df = new DecimalFormat("#0.00");
        while(eingezahlterGesamtbetrag < zuZahlen)
        {
            //Aufgabe 02:
            System.out.println("Noch zu zahlen: " + df.format((zuZahlen - eingezahlterGesamtbetrag)) + "Euro");
            System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
            eingeworfeneMünze = tastatur.nextDouble();
            eingezahlterGesamtbetrag += eingeworfeneMünze;
        }
        return eingezahlterGesamtbetrag - zuZahlen;
    }
    static void fahrkartenAusgeben(){
        // Fahrscheinausgabe
        // -----------------
        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
            System.out.print("=");
            try {
                Thread.sleep(250);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        System.out.println("\n\n");
    }
    static void rueckgeldAusgeben() {
        double rückgabebetrag;
        DecimalFormat df = new DecimalFormat("#0.00");

        // Rückgeldberechnung und -Ausgabe
        // -------------------------------
        rückgabebetrag = fahrkartenBezahlen(fahrkartenbestellungErfassen());
        //Zum beenden muss 1 Karte, 1 Euro und 187 Euro eingegeben werden
        if (rückgabebetrag != 186) {
            fahrkartenAusgeben();
            if (rückgabebetrag > 0.0) {
                System.out.printf("Der Rückgabebetrag in Höhe von " + df.format(rückgabebetrag) + " EURO");
                System.out.println(" wird in folgenden Münzen ausgezahlt:");

                while (rückgabebetrag >= 2.0) // 2 EURO-Münzen
                {
                    System.out.println("2 EURO");
                    rückgabebetrag -= 2.0;
                }
                while (rückgabebetrag >= 1.0) // 1 EURO-Münzen
                {
                    System.out.println("1 EURO");
                    rückgabebetrag -= 1.0;
                }
                while (rückgabebetrag >= 0.5) // 50 CENT-Münzen
                {
                    System.out.println("50 CENT");
                    rückgabebetrag -= 0.5;
                }
                while (rückgabebetrag >= 0.2) // 20 CENT-Münzen
                {
                    System.out.println("20 CENT");
                    rückgabebetrag -= 0.2;
                }
                while (rückgabebetrag >= 0.1) // 10 CENT-Münzen
                {
                    System.out.println("10 CENT");
                    rückgabebetrag -= 0.1;
                }
                while (rückgabebetrag >= 0.049)// 5 CENT-Münzen
                {
                    System.out.println("5 CENT");
                    rückgabebetrag -= 0.049;
                }

            }

            System.out.println("\nVergessen Sie nicht, den Fahrschein\n" +
                    "vor Fahrtantritt entwerten zu lassen!\n" +
                    "Wir wünschen Ihnen eine gute Fahrt.\n\n");
            rueckgeldAusgeben();
        }
    }
}